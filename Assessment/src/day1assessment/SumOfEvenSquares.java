package day1assessment;

public class SumOfEvenSquares {
public void findSumOfEven(int num){
	int last=0,sum=0;
	while (num!=0) {
		last=num%10;
		if (last%2==0) {
			sum=sum+(last*last);	
		}
		num=num/10;
	}
	System.out.println(sum);
	
}
}
