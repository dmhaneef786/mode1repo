package day1assessment;

public class FindOddDigitSum {
	public void oddDigitSum(int num){
		int lastDigit = 0, sum = 0;
		while (num != 0) {
			lastDigit = num % 10;
			if (lastDigit % 2 != 0) {
				sum = sum + lastDigit;

			}
			num = num / 10;
		}
		if (sum % 2 == 0) {
			System.out.println(-1);
		} else {
			System.out.println(1);
		}
	}
}
