package com.day4assessment;

public class Circle extends Shape {

	int radius;
	float pi = (float) 3.14;
	
	public Circle(int radius) {
		super();
		this.radius = radius;
	}
	public Circle() {
		// TODO Auto-generated constructor stub
	}
	public int getRadius() {
		return radius;
	}

public float calculateArea(){
		return pi*radius*radius;
	
}
}

