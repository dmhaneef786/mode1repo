//count all vowels in the string

package com.day2assessment;
import java.util.Scanner;

public class CountVowels {

	public static int vowels(String str){
		char[] vowels = {'a', 'e','i','o','u'};
		int count = 0;
		str = str.toLowerCase();
		char[] letters = str.toCharArray();
		for(int i = 0; i < letters.length; i++){
		for(int j = 0; j < vowels.length; j++){
		if(letters[i] == vowels[j]){
		count++;
		}
		}
		}
		return count;
		}

		@SuppressWarnings("resource")
		public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.println("INPUT A STRING");
		String string = in.nextLine();

		System.out.println("Number of  vowels in that string: " + vowels(string));

		}

}
