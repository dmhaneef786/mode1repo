//middle character among the string.

package com.day2assessment;

import java.util.Scanner;

public class MiddleCharacterString {

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.println("Enter the String");
		String str = in.next();
		System.out.print("The middle character in the string: " + middle(str) + "\n");
	}

	public static String middle(String str) {
		int position;
		int length;
		if (str.length() % 2 == 0) {
			position = str.length() / 2 - 1;
			length = 2;
		} else {
			position = str.length() / 2;
			length = 1;
		}
		return str.substring(position, position + length);
	}
}
