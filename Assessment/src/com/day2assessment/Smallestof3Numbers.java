//smallest among 3 numbers

package com.day2assessment;
import java.util.Scanner;
import java.lang.Math;

public class Smallestof3Numbers {

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
	        System.out.print("Input the first number: ");
	        double x = in.nextDouble();
	        System.out.print("Input the Second number: ");
	        double y = in.nextDouble();
	        System.out.print("Input the third number: ");
	        double z = in.nextDouble();
	        System.out.print("The smallest value is " + minfunction(x, y, z)+"\n" );
	    }

	   public static double minfunction(double x, double y, double z)
	    {
	        return Math.min(Math.min(x, y), z);
	    

	}
	 

}
