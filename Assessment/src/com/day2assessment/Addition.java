package com.day2assessment;

public class Addition {

	static void add(int... numbers) {
		int sum = 0;
		for (int num : numbers) {
			if (sum != 0) {
				System.out.print("+");
			}
			sum += num;
			System.out.print(num);
		}
		System.out.println("=" + sum);
	}

	public static void main(String args[]) {


		add(1,2);
		add(1,2,3);
		add(1,2,3,4,5);
		add(1,2,3,4,5,6);
		
	}
}
