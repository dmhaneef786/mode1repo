//write a Java program to search for an element of an integer array of 10 elements.

package com.day3assessment;
import java.util.Scanner;

public class SearchingElementIn10 {

	@SuppressWarnings("resource")
	public static void main(String[] args) {
			int n,i,flag=0,x;
			System.out.print("Enter the no.of elements: ");
			Scanner s = new Scanner(System.in);
			n = s.nextInt();
			int a[] = new int[n];
			System.out.println("Enter all the elements:");
			for(i = 0; i < n; i++)
	        {
	            a[i] = s.nextInt();
	        }
	        System.out.print("Enter the element you want to find:");
	        x = s.nextInt();
	        for(i = 0; i < n; i++)
	        {
	            if(a[i] == x)
	            {
	                flag = 1;
	                break;
	            }
	            else
	            {
	                flag = 0;
	            }
	        }
	        if(flag == 1)
	        {
	            System.out.println("Element found at position:"+(i + 1));
	        }
	        else
	        {
	            System.out.println("Element not found");
	        }
	    }
	}

