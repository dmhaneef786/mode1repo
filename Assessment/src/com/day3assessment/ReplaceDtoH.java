//Write a Java program to replace all the 'd' occurrence characters with �h� characters in each string

package com.day3assessment;

import java.util.Scanner;


public class ReplaceDtoH {
	@SuppressWarnings("resource")
	public static void main(String args[]) {

		System.out.println("enter the word:");
		Scanner scanner = new Scanner(System.in);
		String val = scanner.next();
		String new_str = val.replace('d', 'h');
		System.out.println("replace the d to h in the word is : " + new_str);
	}

}
