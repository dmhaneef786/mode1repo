//Given a string , print �Yes� if it is a palindrome, print �No� otherwise.

package com.day3assessment;
import java.util.Scanner;

public class StringPalindrome {
	@SuppressWarnings("resource")
	public static void main(String[] args) {
		System.out.println("enter the string:");
		Scanner sc = new Scanner(System.in);
		String A = sc.next();
		String reverse = new StringBuffer(A).reverse().toString();
		if (A.equals(reverse))
			System.out.println("Yes");
		else
			System.out.println("No");

	}
}
