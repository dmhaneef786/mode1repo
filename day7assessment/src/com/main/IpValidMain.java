package com.main;

import java.util.Scanner;

import com.model.UserIpCode;

public class IpValidMain {

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		String ipAddress = sc.nextLine();
		
		boolean b = UserIpCode.ipValidator(ipAddress);
		if (b == true)
			System.out.println("Valid");
		else
			System.out.println("Invalid");
	}
}